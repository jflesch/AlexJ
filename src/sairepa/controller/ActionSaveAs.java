package sairepa.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import jp.gr.java_conf.dangan.util.lha.LhaHeader;
import jp.gr.java_conf.dangan.util.lha.LhaOutputStream;

import sairepa.model.Model;
import sairepa.view.SplashScreen;
import sairepa.view.View;

public class ActionSaveAs implements ActionListener {
    private Model model;
    private View view;
    private Controller controller;

    public ActionSaveAs(final Model model, final View view, final Controller controller) {
        this.model = model;
        this.view = view;
        this.controller = controller;
    }

    public static class ZipLhaFileFilter extends FileFilter {
        public ZipLhaFileFilter() { }
        public boolean accept(final File file) {
            return (file.isDirectory()
                    || file.getName().toLowerCase().endsWith(".zip")
                    || file.getName().toLowerCase().endsWith(".lzh"));
        }

        public String getDescription() {
            return "Fichier ZIP/LZH";
        }
    }

    public static class DbaseFileFilter implements java.io.FileFilter {
        public DbaseFileFilter() { }

        public boolean accept(final File f) {
            return (f.getName().toLowerCase().endsWith(".dbt")
                    || f.getName().toLowerCase().endsWith(".dbf"));
        }
    }

    private static void zip(final ZipOutputStream zout, final File f) throws IOException {
        zout.putNextEntry(new ZipEntry(f.getName()));
        FileInputStream in = new FileInputStream(f);

        try {
            byte[] data = new byte[32768];
            int r;
            while ((r = in.read(data)) >= 0) {
                zout.write(data, 0, r);
            }
        } finally {
            in.close();
        }
    }

    private void saveZip(final File f) {
        ZipOutputStream zout;
        try {
            if (f.exists()) {
                f.delete();
            }

            FileOutputStream fout = new FileOutputStream(f);
            zout = new ZipOutputStream(fout);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            for (File file : model.getProjectDir().listFiles(new DbaseFileFilter())) {
                zip(zout, file);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                zout.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private static void lha(final LhaOutputStream zout, final File f) throws IOException {
        zout.putNextEntry(new LhaHeader(f.getName()));
        FileInputStream in = new FileInputStream(f);

        try {
            byte[] data = new byte[32768];
            int r;
            while ((r = in.read(data)) >= 0) {
                zout.write(data, 0, r);
            }
        } finally {
            in.close();
        }
    }

    private void saveLha(final File f) {
        LhaOutputStream zout;
        try {
            if (f.exists()) {
                f.delete();
            }

            FileOutputStream fout = new FileOutputStream(f);
            zout = new LhaOutputStream(fout);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            for (File file : model.getProjectDir().listFiles(new DbaseFileFilter())) {
                lha(zout, file);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                zout.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void save() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new ZipLhaFileFilter());
        fileChooser.setSelectedFile(new File(model.getProjectDir().getName() + ".zip"));
        if (fileChooser.showSaveDialog(view.getMainWindow()) != JFileChooser.APPROVE_OPTION) {
            return;
        }
        File f = fileChooser.getSelectedFile();

        SplashScreen ss = new SplashScreen(view.getMainWindow(),
                                           "Sauvegarde", null);
        ss.start();
        try {
            model.save(ss);

            ss.setProgression(90, "Creation du ZIP");
            if (f.getName().toLowerCase().endsWith(".lzh")) {
                saveLha(f);
            } else {
                saveZip(f);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            ss.stop();
        }
    }

    public void actionPerformed(final ActionEvent e) {
        save();
    }
}
