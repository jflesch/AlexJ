package sairepa.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import sairepa.model.Model;
import sairepa.view.SplashScreen;
import sairepa.view.View;

public class ActionSave implements ActionListener {
    private Model model;
    private View view;
    private Controller controller;

    public ActionSave(final Model model, final View view, final Controller controller) {
        this.model = model;
        this.view = view;
        this.controller = controller;
    }

    public void save() {
        SplashScreen ss = new SplashScreen(view.getMainWindow(),
                                           "Sauvegarde", null);
        ss.start();
        try {
            model.save(ss);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            ss.stop();
        }
    }

    public void actionPerformed(final ActionEvent e) {
        save();
    }
}
