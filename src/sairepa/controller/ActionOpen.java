package sairepa.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import jp.gr.java_conf.dangan.util.lha.LhaFile;
import jp.gr.java_conf.dangan.util.lha.LhaHeader;

import sairepa.model.Model;
import sairepa.model.ProgressionObserver;
import sairepa.model.Util;
import sairepa.view.ErrorMessage;
import sairepa.view.SplashScreen;
import sairepa.view.View;

public class ActionOpen implements ActionListener {
    private Model model;
    private View view;
    private Controller controller;

    public ActionOpen(final Model model, final View view, final Controller controller) {
        this.model = model;
        this.view = view;
        this.controller = controller;
    }

    private void openZip(final File f) throws IOException {
        ZipFile zip = new ZipFile(f);

        try {
            Enumeration<? extends ZipEntry> entries = zip.entries();

            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                InputStream in = zip.getInputStream(entry);

                System.out.println("Restoring: " + entry.getName().toString());
                try {
                    FileOutputStream out = new FileOutputStream(Util.getFile(model.getProjectDir(), entry.getName()));
                    try {
                        byte[] buffer = new byte[32768];
                        int nBytes;

                        while ((nBytes = in.read(buffer)) > 0) {
                            out.write(buffer, 0, nBytes);
                        }
                    } finally {
                        out.close();
                    }
                } finally {
                    in.close();
                }
            }
        } finally {
            zip.close();
        }
    }

    @SuppressWarnings("rawtypes") // old library ... old API ...
    private void openLha(final File f) throws IOException {
        LhaFile lha = new LhaFile(f);

        try {
            Enumeration entries = lha.entries();

            while (entries.hasMoreElements()) {
                LhaHeader entry = ((LhaHeader) entries.nextElement());
                InputStream in = lha.getInputStream(entry);

                System.out.println("Restoring: " + entry.getPath().toString());
                try {
                    FileOutputStream out = new FileOutputStream(Util.getFile(model.getProjectDir(), entry.getPath()));
                    try {
                        byte[] buffer = new byte[32768];
                        int nBytes;

                        while ((nBytes = in.read(buffer)) > 0) {
                            out.write(buffer, 0, nBytes);
                        }
                    } finally {
                        out.close();
                    }
                } finally {
                    in.close();
                }
            }
        } finally {
            lha.close();
        }
    }

    public void open() {
        if (JOptionPane.showConfirmDialog(view.getMainWindow(),
                                          "Attention, la restauration ecrasera tout vos changements. Etes-vous sur ?",
                                          "Etes-vous sur ?",
                                          JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
            return;
        }

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new ActionSaveAs.ZipLhaFileFilter());
        if (fileChooser.showOpenDialog(view.getMainWindow()) != JFileChooser.APPROVE_OPTION) {
            return;
        }
        File f = fileChooser.getSelectedFile();

        System.out.println("Will restore: " + f.toString());

        view.getMainWindow().closeAllViewers();
        view.getMainWindow().setVisible(false);

        SplashScreen ss = new SplashScreen(view.getMainWindow(), "Ouverture", null);
        ss.start();
        ss.setProgression(0, "Unziping");
        try {
            model.close(ProgressionObserver.DUMB_OBSERVER);

            if (f.getName().toLowerCase().endsWith(".lzh")) {
                openLha(f);
            } else {
                openZip(f);
            }

            model.init(ss);
        } catch (Exception e) { /* wouldn't miss the runtime ones also */
            ErrorMessage.displayError(e);
            throw new RuntimeException(e);
        } finally {
            ss.stop();
        }

        System.out.println(f.toString() + " restored");

        view.getMainWindow().setVisible(true);
    }

    public void actionPerformed(final ActionEvent e) {
        open();
    }
}
