package sairepa.gui;

import java.net.URL;
import javax.swing.ImageIcon;

public final class IconBox {
    public static final ImageIcon minTabClose = loadIcon("tab-close.png");
    public static final ImageIcon actList     = loadIcon("act-list.png");
    public static final ImageIcon act         = loadIcon("act.png");
    public static final ImageIcon up          = loadIcon("up.png");
    public static final ImageIcon down        = loadIcon("down.png");
    public static final ImageIcon search      = loadIcon("search.png");
    public static final ImageIcon quit        = loadIcon("quit.png");
    public static final ImageIcon print       = loadIcon("print.png");
    public static final ImageIcon warning     = loadIcon("warning.png");
    public static final ImageIcon fileSave    = loadIcon("filesave.png");
    public static final ImageIcon fileOpen    = loadIcon("fileopen.png");
    public static final ImageIcon open        = loadIcon("open.png");
    public static final ImageIcon copy        = loadIcon("copy.png");
    public static final ImageIcon cut         = loadIcon("cut.png");
    public static final ImageIcon paste       = loadIcon("paste.png");
    public static final ImageIcon add         = loadIcon("list-add.png");
    public static final ImageIcon remove      = loadIcon("list-remove.png");


    private IconBox() { }

    private static ImageIcon loadIcon(final String fileName) {
        URL url;
        ClassLoader classLoader;

        classLoader = IconBox.class.getClassLoader();
        if (classLoader == null) {
            throw new RuntimeException("Icon '"+fileName+"' not found ! (ClassLoader)");
        }

        url = classLoader.getResource(fileName);
        if (url == null) {
            throw new RuntimeException("Icon '"+fileName+"' not found ! (Resource)");
        }

        return new ImageIcon(url);
    }
}
