package sairepa.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import sairepa.model.structs.BaptismListFactory;
import sairepa.model.structs.BirthListFactory;
import sairepa.model.structs.ConfirmationListFactory;
import sairepa.model.structs.DeceaseListFactory;
import sairepa.model.structs.NotarialDeceaseListFactory;
import sairepa.model.structs.SepulchreListFactory;
import sairepa.model.structs.UnionListFactory;
import sairepa.model.structs.WeddingContractListFactory;
import sairepa.model.structs.WeddingListFactory;


public class Model {
    private final File projectDir;
    private final DbHandler db;
    private final ClientFile clientFile;
    private final ActListFactoryLayout factories;
    private final BackupManager backupManager;

    // dirty singleton
    private static final PrncvDb PRNCVDB = new PrncvDb();

    protected Model(final File projectDir, final ClientFile clientFile) throws IOException, FileNotFoundException {
        this.projectDir = projectDir;
        this.clientFile = clientFile;

        db = new DbHandler();

        factories = new ActListFactoryLayout(
            this,
            new String[] {
                "Pr\351-r\351volution",
                "Etat-civil",
                "Actes notari\351s",
            },
            new DbActListFactory[][] {
                new DbActListFactory[] {
                    new BaptismListFactory(this, projectDir),
                    new WeddingListFactory(this, projectDir),
                    new SepulchreListFactory(this, projectDir),
                    new ConfirmationListFactory(this, projectDir),
                },
                new DbActListFactory[] {
                    new BirthListFactory(this, projectDir),
                    new UnionListFactory(this, projectDir),
                    new DeceaseListFactory(this, projectDir),
                },
                new DbActListFactory[] {
                    new WeddingContractListFactory(this, projectDir),
                    new NotarialDeceaseListFactory(this, projectDir),
                },
            }
        );

        backupManager = new BackupManager(projectDir);
    }

    public File getProjectDir() {
        return projectDir;
    }

    public ClientFile getClientFile() {
        return clientFile;
    }

    public static PrncvDb getPrncvDb() {
        return PRNCVDB;
    }

    public DbHandler getDb() {
        return db;
    }

    public ActListFactoryLayout getFactories() {
        return factories;
    }

    public BackupManager getBackupManager() {
        return backupManager;
    }

    /**
     * @param obs can't be null ; pass a dumb object if you need to
     */
    public void init(final ProgressionObserver obs) throws IOException {
        obs.setProgression(0, "Initialisation de la base de donn\351es ...");
        db.connect(projectDir.getName());

        int nmb = factories.getNumberOfFactories() + 1;
        int i = 1;

        for (DbActListFactory factory : factories) {
            obs.setProgression(i * 90 / nmb,
                               "Chargement de '" + factory.getDbf().getName() + "' ...");
            factory.init(db);
            i++;
        }

        obs.setProgression(95, "Recherche de sauvegardes ...");
        backupManager.init();
    }

    /**
     * @param obs can't be null ; pass a dumb object if you need to
     */
    public void save(final ProgressionObserver obs) throws IOException {
        int nmb = factories.getNumberOfFactories() + 1;
        int i = 0;

        for (DbActListFactory factory : factories) {
            obs.setProgression(i * 90 / nmb,
                               "Ecriture de '" + factory.getDbf().getName() + "' ...");
            factory.save();
            i++;
        }

        obs.setProgression(95, "Sauvegarde ...");
        backupManager.doBackup();
    }

    /**
     * @param obs can't be null ; pass a dumb object if you need to
     */
    public void close(final ProgressionObserver obs) throws IOException {
        obs.setProgression(99, "Fermeture de la base de donn\351es ...");
        db.disconnect();
    }
}
