package sairepa.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public interface AutoCompleter {
    int DEFAULT_MAX_DISTANCE = 10;
    int DEFAULT_MIN_SRC_LENGTH = 2;

    List<String> getSuggestions(ActEntry entry, String initialString);

    /**
     * DefaultAutoCompleter looks in every similar field of the DB.
     */
    class DefaultAutoCompleter implements AutoCompleter {
        private final String fieldName;

        public DefaultAutoCompleter(final String fieldName) {
            this.fieldName = fieldName;
        }

        public List<String> getSuggestions(final ActEntry entry, final String initialString) {
            if (initialString == null || initialString.trim().length() < DEFAULT_MIN_SRC_LENGTH) {
                return new ArrayList<String>();
            }

            ArrayList<String> suggestions = new ArrayList<String>();

            // TODO

            Collections.sort(suggestions);
            // TODO: removeDoubles(suggestions);
            return suggestions;
        }
    }
}
