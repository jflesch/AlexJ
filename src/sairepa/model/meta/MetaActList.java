package sairepa.model.meta;

import java.util.List;

import sairepa.model.*;


public class MetaActList implements ActList {
    private ActListDbObserver observer = new DumbDbObserver();
    private MetaActListFactory factory;
    private String name;

    public MetaActList(final MetaActListFactory factory, final String name) {
        this.factory = factory;
        this.name = name;
    }

    public ActListFactory getFactory() {
        return this.factory;
    }

    public String getName() {
        return this.name;
    }

    public int getRowCount() {
        /* TODO */
        return 0;
    }

    public int getFileId() {
        return -1;
    }

    public void setActListDbObserver(ActListDbObserver obs) {
        this.observer = obs;
    }

    public ActListDbObserver getActListDbObserver() {
        return this.observer;
    }

    public Act getAct(int row) {
        /* TODO */
        return null;
    }

    public int getActVisualRow(Act a) {
        /* TODO */
        return -1;
    }

    public List<Act> getAllActs() {
        /* TODO */
        return null;
    }

    public ActList getSortedActList(List<ActSorting> sortingRule) {
        /*
        return new SortedActList(
                this.factory.getModel().getDb(),
                this, sortingRule);
        */
        // TODO
        assert (false);
        return null;
    }

    public void insert(Act act) {
        throw new UnsupportedOperationException();
    }

    public void insert(Act act, int row) {
        throw new UnsupportedOperationException();
    }

    public void delete(Act act) {
        throw new UnsupportedOperationException();
    }

    public void update(Act act) {
    }

    public void save() {
        // TODO
    }

    public ActListIterator iterator() {
        /* TODO */
        return null;
    }

    public Act createAct() {
        throw new UnsupportedOperationException();
    }

    public void refresh() {
        /* TODO */
    }

    public void refresh(Act a) {
        refresh();
    }
}
