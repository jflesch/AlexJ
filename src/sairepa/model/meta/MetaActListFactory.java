package sairepa.model.meta;

import java.util.Set;

import sairepa.model.*;

public class MetaActListFactory implements ActListFactory {
    private String name;
    private ActListFactoryLayout actLists;
    private Set<ActField> fields;

    public MetaActListFactory(String name, ActListFactoryLayout actLists) {
        this.name = name;
        this.actLists = actLists;
        this.fields = findCommonFields(actLists);
    }

    public static Set<ActField> findCommonFields(ActListFactoryLayout actListFactories) {
        Set<ActField> commonFields = null;

        for (ActListFactory factory : actListFactories) {
            if (commonFields == null) {
                commonFields = factory.getFieldSet();
            } else {
                commonFields.retainAll(factory.getFieldSet());
            }
        }

        System.out.print("Common fields:");
        for (ActField field : commonFields) {
            System.out.print(field.getName() + ", ");
        }
        System.out.println("");

        return commonFields;
    }

    public Model getModel() {
        return this.actLists.getModel();
    }

    public Set<ActField> getFieldSet() {
        return this.fields;
    }

    public FieldLayout getFields() {
        /* TODO */
        return null;
    }

    public ActList getActList(ActList.ActListDbObserver obs) {
        ActList al = new MetaActList(this, name);
        al.setActListDbObserver(obs);
        al.refresh();
        return al;
    }

    public int getFieldId(String name) {
        /* TODO */
        return -1;
    }
}
