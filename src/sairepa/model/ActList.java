package sairepa.model;

import java.util.List;
import java.util.ListIterator;

public interface ActList extends Iterable<Act> {
    ActListFactory getFactory();
    String getName();
    int getRowCount();

    enum DbOp {
        DB_QUERY(),
        DB_FETCH(),
        DB_SORT(),
    }

    /**
     * The obserser is notified each time a long db query
     * or manipulation is started.
     */
    interface ActListDbObserver {
        void startOfJobBatch(String description, int nmbJob);
        void jobUpdate(DbOp job, int currentPosition, int endOfJobPosition);
        void endOfJobBatch();
    }

    void setActListDbObserver(ActListDbObserver obs);
    ActListDbObserver getActListDbObserver();

    interface ActListIterator extends ListIterator<Act> {
        void add(Act a);
        boolean hasNext();
        boolean hasPrevious();
        Act next();
        int nextIndex();
        Act previous();
        Act seek(int position);
        int previousIndex();
        int currentIndex();
        void remove();
        void set(Act a);
    }

    Act getAct(int row);
    int getActVisualRow(Act a);
    List<Act> getAllActs();

    /**
     * @param sortedBy field name ; can be null
     * @return beware: can return this !
     */
    ActList getSortedActList(List<ActSorting> sortingRule);

    void insert(Act act);
    void insert(Act act, int row);
    void delete(Act act);
    void update(Act act);
    ActListIterator iterator();
    Act createAct();
    void refresh();
    void refresh(Act a);

    void save();
}
