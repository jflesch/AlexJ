package sairepa.model;

import java.util.Comparator;


public class ActSorting implements Comparator<Act> {
    private final ActField field;
    private final boolean desc;

    public ActSorting(final ActField sortedBy, final boolean desc) {
        this.field = sortedBy;
        this.desc = desc;
    }

    public String getFieldName() {
        return field.getName();
    }

    public ActField getField() {
        return field;
    }

    public boolean getOrder() {
        return desc;
    }

    public String toString() {
        String str = field.getName();
        if (desc) {
            str += " (desc)";
        }
        return str;
    }

    public int compare(final Act actA, final Act actB) {
        int r;

        if (field == null) {
            r = Integer.compare(actA.getRow(), actB.getRow());
        } else {
            ActEntry entryA = actA.getEntry(field);
            ActEntry entryB = actB.getEntry(field);
            String a = entryA.getValue();
            String b = entryB.getValue();

            try {
                int iA = Integer.parseInt(a);
                int iB = Integer.parseInt(b);
                r = Integer.compare(iA, iB);
            } catch (NumberFormatException exc) {
                r = a.compareTo(b);
            }
        }

        if (desc) {
            r *= -1;
        }
        return r;
    }
}
