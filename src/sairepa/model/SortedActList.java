package sairepa.model;

import java.util.Comparator;
import java.util.List;
import java.util.Vector;


public class SortedActList implements ActList {
    private Object lock;
    private final ActList masterActList;
    private Comparator<Act> sorting;

    private List<Act> allActs = null;
    private int[] positionToActRow; // position = position once sorted
    private int[] actRowToPosition; // position = position once sorted

    public SortedActList(final Object lock,
            final ActList masterActList,
            final Comparator<Act> sorting) {
        this.lock = lock;
        this.masterActList = masterActList;
        this.sorting = sorting;
        Util.check(this.sorting != null);

        refresh();
    }

    public void setActListDbObserver(final ActList.ActListDbObserver obs) {
        masterActList.setActListDbObserver(obs);
    }

    public ActList.ActListDbObserver getActListDbObserver() {
        return masterActList.getActListDbObserver();
    }

    public ActListFactory getFactory() {
        return masterActList.getFactory();
    }

    public String getName() {
        return masterActList.getName();
    }

    public int getRowCount() {
        return masterActList.getRowCount();
    }

    public int getActVisualRow(final Act a) {
        if (actRowToPosition == null) {
            getAllActs();
        }
        return actRowToPosition[a.getRow()];
    }

    public Act getAct(final int position) {
        if (positionToActRow == null) {
            getAllActs();
        }
        int row = positionToActRow[position];
        return masterActList.getAct(row);
    }

    public List<Act> getAllActs() {
        if (allActs != null) {
            return allActs;
        }
        List<Act> acts = masterActList.getAllActs();

        java.util.Date start, stop;
        start = new java.util.Date();

        acts = new Vector<Act>(acts);
        acts.sort(sorting);

        positionToActRow = new int[acts.size()];
        actRowToPosition = new int[acts.size()];
        for (int pos = 0; pos < positionToActRow.length; pos++) {
            Act a = acts.get(pos);
            int row = a.getRow();
            positionToActRow[pos] = row;
            actRowToPosition[row] = pos;
        }

        this.allActs = acts;

        stop = new java.util.Date();
        System.out.println("Took " + Long.toString((stop.getTime() - start.getTime()))
                + " ms to sort " + Integer.toString(getRowCount()) + " elements");
        return allActs;
    }

    /**
     * @return beware: can return this !
     */
    public ActList getSortedActList(final List<ActSorting> sortingRule) {
        return masterActList.getSortedActList(sortingRule);
    }

    public void insert(final Act act) {
        masterActList.insert(act);
    }

    public void insert(final Act act, final int row) {
        throw new UnsupportedOperationException("Can't do");
    }

    public void delete(final Act act) {
        masterActList.delete(act);
    }

    public void update(final Act act) {
        masterActList.update(act);
    }

    public void save() {
        masterActList.save();
    }

    public ActListIterator iterator() {
        return new GenericActListIterator(lock, this);
    }

    public Act createAct() {
        return masterActList.createAct();
    }

    public void refresh() {
        allActs = null;
        if (allActs != null) {
            getAllActs();
        }
    }

    public void refresh(final Act a) {
    }
}
