package sairepa.model;

import java.io.File;
import java.io.IOException;
import java.util.TreeSet;
import java.util.Set;

/**
 * Do the work about importing/exporting DBF files into the db.
 */
public abstract class DbActListFactory implements ActListFactory {
    private FieldLayout fields;
    private ActList actList;
    private Model model;

    private File dbf;
    private File dbt;
    private int fileId; // db specific

    private DbHandler db;

    public DbActListFactory(final Model m, final File dbf, final File dbt, final FieldLayout fields) {
        this.fields = fields;
        this.dbf = dbf;
        this.dbt = dbt;
        this.model = m;
    }

    public Model getModel() {
        return model;
    }

    public File getDbf() {
        return dbf;
    }

    public File getDbt() {
        return dbt;
    }

    public void init(final DbHandler db) throws IOException {
        this.db = db;

        actList = new DbActList(db, this, toString());
    }

    public void save() throws IOException {
        actList.save();
    }

    public Set<ActField> getFieldSet() {
        TreeSet<ActField> fieldSet = new TreeSet<ActField>();
        for (ActField field : fields) {
            fieldSet.add(field);
        }
        return fieldSet;
    }

    public FieldLayout getFields() {
        return fields;
    }

    /**
     * Returns the default act list type, always hitting the DB.
     */
    public ActList getActList(final ActList.ActListDbObserver obs) {
        actList.setActListDbObserver(obs);
        return actList;
    }

    public abstract String toString();
}
