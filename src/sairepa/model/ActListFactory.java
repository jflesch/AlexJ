package sairepa.model;

import java.util.Set;

public interface ActListFactory {
    Model getModel();
    Set<ActField> getFieldSet();
    FieldLayout getFields();
    ActList getActList(ActList.ActListDbObserver obs);
}
