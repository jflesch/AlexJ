package sairepa.model;

import java.io.File;
import java.io.IOException;

public class DbHandler {
    private String project;

    public DbHandler() {
    }

    public void connect(final String projectName) throws IOException {
        // TODO: lock project
    }

    public void disconnect() throws IOException {
        // TODO: unlock project
    }

    private static void lockProject(final String project) {
        File f = new File(project + ".lock");
        boolean b;

        try {
            b = f.createNewFile();
        } catch (IOException e) {
            /* TODO(Jflesch): l10n */
            throw new RuntimeException("Erreur lors de la verification du verrou du projet '" + project + "'.", e);
        }

        if (!b) {
            /* TODO(Jflesch): l10n */
            throw new RuntimeException("Ce projet semble etre deja utilise par une autre instance de " + sairepa.Main.APPLICATION_NAME + ". "
                                       + "Si ce n'est pas le cas, veuillez effacer le fichier '" + project + ".lock' "
                                       + "du repertoire " + sairepa.Main.APPLICATION_NAME);
        } else {
            f.deleteOnExit();
        }
    }

    private static void unlockProject(final String project) {
        File f = new File(project + ".lock");
        f.delete();
    }
}
