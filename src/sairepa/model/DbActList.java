package sairepa.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import net.kwain.fxie.XBaseException;
import net.kwain.fxie.XBaseHeader;
import net.kwain.fxie.XBaseExport;
import net.kwain.fxie.XBaseImport;
import net.kwain.fxie.XBaseValue;


public class DbActList implements ActList {
    private DbActListFactory factory;
    private Object lock;
    private String name;
    private ActList.ActListDbObserver dbObserver = new DumbDbObserver();

    private XBaseImport currentImport = null;
    private int currentRow;

    private List<Act> allActs = null;

    protected DbActList(final Object lock, final DbActListFactory factory, final String name)
            throws IOException {
        this.lock = lock;
        this.factory = factory;
        this.name = name;
        computeRowCount();
    }

    public ActListFactory getFactory() {
        return factory;
    }

    public String getName() {
        return name;
    }

    public void setActListDbObserver(final ActList.ActListDbObserver obs) {
        this.dbObserver = obs;
    }

    public ActListDbObserver getActListDbObserver() {
        return this.dbObserver;
    }

    public void computeRowCount() throws IOException {
        getAllActs();
    }

    public int getRowCount() {
        return getAllActs().size();
    }

    public Act getAct(final int row) {
        List<Act> all = getAllActs();
        if (row < 0 || row >= all.size()) {
            return null;
        }
        return all.get(row);
    }

    public int getActVisualRow(final Act a) {
        /* not sorted, so no problem here */
        return a.getRow();
    }

    public List<Act> getAllActs() {
        if (allActs != null) {
            return allActs;
        }

        java.util.Date start, stop;
        start = new java.util.Date();

        List<Act> acts;

        synchronized (lock) {
            try {
                XBaseImport xbase = new XBaseImport(factory.getDbf(), factory.getDbt());
                try {
                    acts = new Vector<Act>(xbase.available());
                    int row = 0;
                    XBaseHeader header = xbase.getHeader();
                    List<XBaseHeader.XBaseField> xbaseFields = header.getFields();
                    FieldLayout layout = factory.getFields();
                    List<XBaseValue> record;
                    ActField actField;

                    System.out.println(String.format("Loading %d elements", xbase.available()));
                    for (row = 0; xbase.available() > 0; row++) {
                        record = xbase.read();
                        if (record == null) {
                            System.out.println("Unexpected EOF");
                            break;
                        }

                        Map<ActField, String> values = new HashMap<ActField, String>();
                        for (int i = 0; i < xbaseFields.size(); i++) {
                            actField = layout.getField(xbaseFields.get(i).getName());
                            values.put(actField, record.get(i).getHumanReadableValue());
                        }
                        acts.add(new Act(this, values, row));
                    }
                } finally {
                    xbase.close();
                }
            } catch (XBaseException exc) {
                allActs = new Vector<Act>(0);
                System.out.println("XBase exception: " + exc.toString());
                throw new RuntimeException(exc);
            } catch (FileNotFoundException exc) {
                allActs = new Vector<Act>(0);
                System.out.println("File not found: " + exc.toString());
                return allActs;
            }

            allActs = acts;
            stop = new java.util.Date();
            System.out.println("Took " + Long.toString((stop.getTime() - start.getTime()) / 1000)
                    + " seconds to load " + Integer.toString(getRowCount()) + " elements");
        }
        return allActs;
    }

    private class XBaseProvider implements XBaseExport.DataProvider {
        private List<Act> acts;

        XBaseProvider(final List<Act> acts) throws XBaseException {
            this.acts = acts;
        }

        public boolean hasRow(final int row) {
            return (row < acts.size());
        }

        public String getValue(final int row, final XBaseHeader.XBaseField xbaseField) {
            Act act = acts.get(row);
            ActField field = factory.getFields().getField(xbaseField.getName());
            ActEntry entry = act.getEntry(field);
            return entry.getValue();
        }
    }

    private List<XBaseHeader.XBaseField> getDbfFields() throws XBaseException {
        List<XBaseHeader.XBaseField> dbfFields = new Vector<XBaseHeader.XBaseField>();
        for (ActField field : factory.getFields().getFields()) {
            dbfFields.add(field.createDBFField());
        }
        return dbfFields;
    }

    private void writeAllActs() {
        synchronized (lock) {
            getAllActs();

            if (allActs.size() <= 0) {
                System.out.println(getName() + ": No act. Just deleting dbt/dbf files");
                for (File f : new File[] {factory.getDbf(), factory.getDbt()}) {
                    if (f.exists()) {
                        f.delete();
                    }
                }
                return;
            }

            java.util.Date start, stop;
            start = new java.util.Date();

            try {
                XBaseProvider provider = new XBaseProvider(allActs);
                XBaseExport export = new XBaseExport(
                    factory.getDbf(), factory.getDbt(),
                    XBaseHeader.XBaseVersion.XBASE_VERSION_DBASE_IIIP_MEMO,
                    XBaseHeader.XBaseCharset.CHARSET_DOS_USA,
                    getDbfFields(),
                    provider, ' '
                );
                export.write();
            } catch (XBaseException e) {
                throw new RuntimeException("XBaseException while writing the dbf file: " + e.toString(), e);
            }

            stop = new java.util.Date();
            System.out.println("Took " + Long.toString((stop.getTime() - start.getTime()) / 1000)
                    + " seconds to write " + Integer.toString(getRowCount()) + " elements");
        }
    }

    public void insert(final Act act) {
        insert(act, allActs.size());
    }

    public void insert(final Act act, int row) {
        List<Act> acts = getAllActs();
        acts.add(row, act);
        act.setRow(row);

        acts = acts.subList(row + 1, acts.size());
        row += 1;
        for (Act a : acts) {
            a.setRow(row);
            row += 1;
        }
    }

    public void delete(final Act act) {
        int row = act.getRow();

        List<Act> acts = getAllActs();
        acts.remove(row);

        acts = acts.subList(row, acts.size());
        for (Act a : acts) {
            a.setRow(row);
            row += 1;
        }
    }

    public void update(final Act act) {
    }

    public ActListIterator iterator() {
        return new GenericActListIterator(lock, this);
    }

    /**
     * this act is not stored until added to the list.
     */
    public Act createAct() {
        return new Act(this);
    }

    public void refresh() {
    }

    public void refresh(final Act a) {
        a.reload();
    }

    public void save() {
        writeAllActs();
    }

    private class MergedComparable<T> implements Comparator<T> {
        private final List<? extends Comparator<T>> comparators;

        MergedComparable(final List<? extends Comparator<T>> comparators) {
            this.comparators = comparators;
        }

        public int compare(final T a, final T b) {
            int r;
            for (Comparator<T> c : this.comparators) {
                r = c.compare(a, b);
                if (r != 0) {
                    return r;
                }
            }
            return 0;
        }
    }

    public ActList getSortedActList(final List<ActSorting> sortingRule) {
        ActList al = this;
        if (sortingRule.size() > 0) {
            al = new SortedActList(
                lock, this, new MergedComparable<Act>(sortingRule)
            );
        }
        return al;
    }
}
