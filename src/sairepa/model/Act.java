package sairepa.model;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class Act {
    private int row = -1;
    private ActList actList;
    private FieldLayout fields;
    private Map<ActField, ActEntry> entries;

    /**
     * Create a brand new and empty act.
     */
    protected Act(final ActList actList) {
        Util.check(actList != null);

        this.actList = actList;

        entries = new HashMap<ActField, ActEntry>();
        for (ActField field : actList.getFactory().getFields()) {
            entries.put(field, field.createEntry(this));
        }
    }

    /**
     * Instantiate fully an act (no db call).
     */
    public Act(final ActList actList,
               final Map<ActField, String> entryValues,
               final int row) {
        Util.check(actList != null);

        this.actList = actList;
        this.fields = fields;
        this.entries = new HashMap<ActField, ActEntry>();
        this.row = row;

        for (Map.Entry<ActField, String> entryVal : entryValues.entrySet()) {
            this.entries.put(entryVal.getKey(),
                             new ActEntry(this, entryVal.getKey(), entryVal.getValue()));
        }
    }

    /**
     * Already existing act loaded from the db.
     */
    public Act(final ActList actList, final int row) throws IOException {
        Util.check(actList != null);
        Util.check(row >= 0);

        this.row = row;
        this.actList = actList;

        reload();
    }

    public ActList getActList() {
        return actList;
    }

    public Collection<ActEntry> getEntries() {
        return entries.values();
    }

    public ActEntry getEntry(final ActField field) {
        return entries.get(field);
    }

    public void setRow(final int row) {
        setRow(row, true);
    }

    protected void setRow(final int row, final boolean reload) {
        this.row = row;

        if (reload) {
            reload();
        }
    }

    public int getRow() {
        return row;
    }

    /**
     * reload according to the row. may have some side effects
     */
    public void reload() {
        if (this.row < 0) {
            return;
        }
        Act actual = actList.getAct(this.row);
        if (actual == null) {
            return;
        }

        // steal its entries
        Collection<ActEntry> actualEntries = new Vector<ActEntry>(actual.getEntries());
        entries = new HashMap<ActField, ActEntry>();
        for (ActEntry entry : actualEntries) {
            entries.put(entry.getField(), entry);
        }
    }

    public void update() {
        actList.update(this);
    }

    protected void delete() {
        actList.delete(this);
    }

    public boolean validate() {
        return actList.getFactory().getFields().validate(this);
    }
}
