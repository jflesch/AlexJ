package sairepa.model;

import java.util.List;

public class ReverseActList implements ActList {
    private final Object lock;
    private final ActList masterActList;

    protected ReverseActList(final Object lock, final ActList masterActList) {
        this.lock = lock;
        this.masterActList = masterActList;
    }

    public void setActListDbObserver(final ActList.ActListDbObserver obs) {
        masterActList.setActListDbObserver(obs);
    }

    public ActList.ActListDbObserver getActListDbObserver() {
        return masterActList.getActListDbObserver();
    }

    public ActListFactory getFactory() {
        return masterActList.getFactory();
    }

    public String getName() {
        return masterActList.getName();
    }

    public int getRowCount() {
        return masterActList.getRowCount();
    }

    public int getActVisualRow(final Act a) {
        return (getRowCount() - 1) - masterActList.getActVisualRow(a);
    }

    public Act getAct(final int position) {
        return masterActList.getAct((getRowCount() - 1) - position);
    }

    public List<Act> getAllActs() {
        List<Act> acts = masterActList.getAllActs();
        java.util.Collections.reverse(acts);
        return acts;
    }

    /**
     * @return beware: can return this !
     */
    public ActList getSortedActList(final List<ActSorting> sortingRule) {
        return masterActList.getSortedActList(sortingRule);
    }

    public void insert(final Act act) {
        masterActList.insert(act);
    }

    public void insert(final Act act, final int row) {
        /* row definition is not clear here, better not do anything */
        throw new UnsupportedOperationException("Can't do");
    }

    public void delete(final Act act) {
        masterActList.delete(act);
    }

    public void update(final Act act) {
        masterActList.update(act);
    }

    public ActListIterator iterator() {
        return new GenericActListIterator(lock, this);
    }

    public Act createAct() {
        return masterActList.createAct();
    }

    public void refresh() {
        masterActList.refresh();
    }

    public void refresh(final Act a) {
        masterActList.refresh(a);
    }

    public void save() {
        masterActList.save();
    }
}

