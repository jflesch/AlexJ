package sairepa.view;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;

import sairepa.model.ActList;
import sairepa.model.ActListFactory;
import sairepa.model.ActListFactoryLayout;

public class ActViewerFactory implements ViewerFactory {
    public final static String NAME = "Fiche";

    public ActViewerFactory() { }

    public String getName() {
        return NAME;
    }

    public boolean acceptActList() {
        return true;
    }

    public boolean acceptModel() {
        return false;
    }

    public Viewer createViewer(MainWindow mainWindow, ActList list) {
        assert(list != null);
        return new ActViewer(mainWindow, list);
    }

    public Viewer createViewer(MainWindow mainWindow, ActListFactoryLayout layout) {
        throw new UnsupportedOperationException();
    }
}
