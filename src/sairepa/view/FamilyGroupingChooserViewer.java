package sairepa.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import sairepa.gui.IconBox;
import sairepa.model.Act;
import sairepa.model.ActField;
import sairepa.model.ActList;
import sairepa.model.ActListFactory;
import sairepa.model.ActListFactoryLayout;
import sairepa.model.ActSorting;
import sairepa.model.DumbDbObserver;
// import sairepa.model.meta.MetaActList;
import sairepa.model.meta.MetaActListFactory;

public class FamilyGroupingChooserViewer extends Viewer implements ActionListener {
    private static final ActSorting[][] PREDEFINED_SORTINGS = {
        // TODO
        /*
        {
            new ActSorting("NOM2CV", false),
            new ActSorting("PRN2CV", false),
            new ActSorting("MX", false),
            new ActSorting("NOM3CV", false),
            new ActSorting("PRN3CV", false),
        },
        {
            new ActSorting("NOM3CV", false),
            new ActSorting("PRN3CV", false),
            new ActSorting("NOM2CV", false),
            new ActSorting("PRN2CV", false),
            new ActSorting("MX", false),
        },
        {
            new ActSorting("PRN2CV", false),
            new ActSorting("PRN3CV", false),
            new ActSorting("NOM2CV", false),
            new ActSorting("NOM3CV", false),
            new ActSorting("MX", false),
        },
        {
            new ActSorting("PRN3CV", false),
            new ActSorting("PRN2CV", false),
            new ActSorting("NOM3CV", false),
            new ActSorting("NOM2CV", false),
            new ActSorting("MX", false),
        },
        */
    };

    private static final long serialVersionUID = 1;

    private ViewerProxy proxy;
    private Set<ActField> possibleFields;

    private GlobalSortingChooser globalChooser;
    private List<SingleSortingChooser> singleChoosers;

    private JButton addButton;
    private JButton validButton;

    private ActListFactoryLayout actListFactories;
    private MetaActListFactory actListFactory;


    public FamilyGroupingChooserViewer(final ActListFactoryLayout actListFactories, final ViewerProxy proxy) {
        super(FamilyViewerFactory.NAME);
        this.proxy = proxy;

        this.actListFactories = actListFactories;
        this.actListFactory = new MetaActListFactory(FamilyViewerFactory.NAME, actListFactories);
        possibleFields = this.actListFactory.getFieldSet();

        singleChoosers = new Vector<SingleSortingChooser>();
        SingleSortingChooser chooser = new SingleSortingChooser(this.possibleFields);
        singleChoosers.add(chooser);

        Vector<GlobalSorting> allPredefined = new Vector<GlobalSorting>();
        for (ActSorting[] predefinedSorting : PREDEFINED_SORTINGS) {
            Vector<ActSorting> sortingsList = new Vector<ActSorting>();
            for (ActSorting field : predefinedSorting) {
                sortingsList.add(field);
            }
            GlobalSorting sortings = new GlobalSorting(sortingsList);
            allPredefined.add(sortings);
        }
        allPredefined.add(new GlobalSorting(null));
        globalChooser = new GlobalSortingChooser(allPredefined);
        globalChooser.getChooser().addActionListener(this);

        rebuildUI();
    }

    private final class GlobalSorting {
        private List<ActSorting> sorting;
        private String sortingStr;

        private GlobalSorting(final List<ActSorting> sorting) {
            this.sorting = sorting;
            if (this.sorting == null) {
                this.sortingStr =  "Tri personnalisé";
            } else {
                this.sortingStr = "";
                for (ActSorting field : sorting) {
                    if (!"".equals(this.sortingStr)) {
                        this.sortingStr += " > ";
                    }
                    this.sortingStr += field.getFieldName();
                }
            }
        }

        public List<ActSorting> getSorting() {
            return sorting;
        }

        public String toString() {
            return this.sortingStr;
        }
    }

    private final class GlobalSortingChooser {
        private final JComboBox<GlobalSorting> sortingChooser;

        private GlobalSortingChooser(final List<GlobalSorting> sortings) {
            sortingChooser = new JComboBox<GlobalSorting>(new Vector<GlobalSorting>(sortings));
            sortingChooser.setPreferredSize(new java.awt.Dimension(500, 30));
        }

        public List<ActSorting> getSorting() {
            int idx = sortingChooser.getSelectedIndex();
            GlobalSorting s = sortingChooser.getItemAt(idx);
            return s.getSorting();
        }

        public JComboBox<GlobalSorting> getChooser() {
            return sortingChooser;
        }
    }

    private class SingleSortingChooser {
        private final JComboBox<ActField> fieldChooser;
        private final JCheckBox orderChooser;
        private final JButton removeButton;

        SingleSortingChooser(final Set<ActField> possibleFields) {
            fieldChooser = new JComboBox<ActField>(new Vector<ActField>(possibleFields));
            fieldChooser.setPreferredSize(new java.awt.Dimension(300, 20));
            orderChooser = new JCheckBox("", false);
            removeButton = new JButton(IconBox.remove);
            removeButton.setToolTipText("Retirer ce critère de tri");
        }

        public JComponent getFieldChooser() {
            return fieldChooser;
        }

        public JComponent getOrderChooser() {
            return orderChooser;
        }

        public JButton getRemoveButton() {
            return removeButton;
        }

        public void setEnabled(final boolean e) {
            fieldChooser.setEnabled(e);
            orderChooser.setEnabled(e);
            removeButton.setEnabled(e);
        }

        public ActSorting getSorting() {
            int idx = fieldChooser.getSelectedIndex();
            return new ActSorting(
                    fieldChooser.getItemAt(idx),
                    orderChooser.isSelected());
        }
    }

    private void setSingleChoosersEnabled(final boolean e) {
        for (SingleSortingChooser chooser : singleChoosers) {
            chooser.setEnabled(e);
        }
        addButton.setEnabled(e);
    }

    public void actionPerformed(final ActionEvent e) {
        if (e.getSource() == validButton) {
            Vector<ActSorting> sortingRule = new Vector<ActSorting>();
            for (SingleSortingChooser chooser : singleChoosers) {
                sortingRule.add(chooser.getSorting());
            }
            ActListFactory alf = new MetaActListFactory(FamilyViewerFactory.NAME, actListFactories);
            /* TODO(Jflesch): DbObserver */
            ActList al = alf.getActList(new DumbDbObserver());
            al = al.getSortedActList(sortingRule);
            proxy.setViewer(new ActListViewer(al, false /* allowReordering */),
                    true /* maxSize */);
        } else if (e.getSource() == addButton) {
            SingleSortingChooser chooser = new SingleSortingChooser(this.possibleFields);
            singleChoosers.add(chooser);
            rebuildUI();
        } else  if (e.getSource() == globalChooser.getChooser()) {
            boolean enableSingles = (globalChooser.getSorting() == null);
            setSingleChoosersEnabled(enableSingles);
        } else {
            for (SingleSortingChooser chooser : singleChoosers) {
                if (e.getSource() == chooser.getRemoveButton()) {
                    singleChoosers.remove(chooser);
                    rebuildUI();
                    break;
                }
            }
        }
    }

    public void refresh() {
        /* nothing to do */
    }

    public void refresh(final Act a) {
        /* nothing to do */
    }

    public String canClose() {
        /* we can always be closed */
        return null;
    }

    public int[] getSelectedActs() {
        /* no act can be selected in this viewer */
        return new int[0];
    }

    public void setSelectedAct(final int act) {
        /* no act can be selected in this viewer */
        return;
    }

    private void rebuildUI() {
        this.removeAll();
        this.setLayout(new GridLayout(1, 1));

        JPanel pouetPanel = new JPanel();
        pouetPanel.setLayout(new BorderLayout(5, 5));

        JPanel omegaPanel = new JPanel();

        omegaPanel.setLayout(new BorderLayout(5, 5));

        JPanel masterPanel = new JPanel(new BorderLayout(5, 5));
        JPanel rightPanel = new JPanel(new BorderLayout(5, 5));

        JPanel orderColumn = new JPanel(new GridLayout(singleChoosers.size() + 3, 1));
        JPanel fieldColumn = new JPanel(new GridLayout(singleChoosers.size() + 3, 1));
        JPanel removeColumn = new JPanel(new GridLayout(singleChoosers.size() + 3, 1));

        /* header */
        fieldColumn.add(new JLabel("Trier par"));
        orderColumn.add(new JLabel("Décroissant"));
        removeColumn.add(new JLabel(""));
        /* choosers */
        int i = 0;
        for (SingleSortingChooser chooser : singleChoosers) {
            orderColumn.add(chooser.getOrderChooser());
            fieldColumn.add(chooser.getFieldChooser());

            if (i == 0) {
                removeColumn.add(new JLabel(""));
            } else {
                removeColumn.add(chooser.getRemoveButton());
                chooser.getRemoveButton().addActionListener(this);
            }
            i++;
        }

        /* validation / adding */
        fieldColumn.add(new JLabel(""));
        orderColumn.add(new JLabel(""));
        this.addButton = new JButton(IconBox.add);
        addButton.setToolTipText("Ajouter un nouveau critère de tri");
        addButton.addActionListener(this);
        removeColumn.add(addButton);

        orderColumn.add(new JLabel(""));
        this.validButton = new JButton("Valider");
        validButton.addActionListener(this);
        fieldColumn.add(validButton);
        removeColumn.add(new JLabel(""));

        masterPanel.add(fieldColumn, BorderLayout.WEST);
        rightPanel.add(orderColumn, BorderLayout.WEST);
        rightPanel.add(removeColumn, BorderLayout.EAST);
        masterPanel.add(rightPanel, BorderLayout.EAST);

        omegaPanel.add(masterPanel, BorderLayout.NORTH);
        omegaPanel.add(new JLabel(""), BorderLayout.CENTER);

        pouetPanel.add(globalChooser.getChooser(), BorderLayout.NORTH);
        pouetPanel.add(omegaPanel, BorderLayout.CENTER);

        this.add(pouetPanel);

        boolean enableSingles = (globalChooser.getSorting() == null);
        setSingleChoosersEnabled(enableSingles);

        proxy.validate();
    }
}
