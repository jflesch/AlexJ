package sairepa.view;

import sairepa.model.ActList;
import sairepa.model.ActListFactoryLayout;

public class FamilyViewerFactory implements ViewerFactory {
    public final static String NAME = "Reg. familial";

    public FamilyViewerFactory() { }

    public String getName() {
        return NAME;
    }

    public boolean acceptActList() {
        return false;
    }

    public boolean acceptModel() {
        return true;
    }

    public Viewer createViewer(MainWindow mainWindow, ActList list) {
        throw new UnsupportedOperationException();
    }

    public Viewer createViewer(MainWindow mainWindow, ActListFactoryLayout layout) {
        ViewerProxy proxy = new ViewerProxy(NAME);
        FamilyGroupingChooserViewer chooser = new FamilyGroupingChooserViewer(layout, proxy);
        proxy.setViewer(chooser, false);
        return proxy;
    }
}
