package sairepa.view;

import sairepa.model.ActList;
import sairepa.model.ActListFactory;
import sairepa.model.ActListFactoryLayout;

public interface ViewerFactory {
    public String getName();

    public boolean acceptActList();
    public boolean acceptModel(); /** --> ActListFactoryLayout */

    /**
     * @param mainWindow just provided if you must create
     *        a dialog, don't touch it directly otherwise.
     */
    public Viewer createViewer(MainWindow mainWindow, ActList list);

    /**
     * @param mainWindow just provided if you must create
     *        a dialog, don't touch it directly otherwise.
     */
    public Viewer createViewer(MainWindow mainWindow, ActListFactoryLayout actListLayout);
}
