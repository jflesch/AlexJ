package sairepa.view;

import sairepa.model.ActList;
import sairepa.model.ActListFactoryLayout;

public class SortedActListViewerFactory implements ViewerFactory {
    public static final String NAME = "Tableau trié";

    public SortedActListViewerFactory() { }

    public String getName() {
        return NAME;
    }

    public boolean acceptActList() {
        return true;
    }

    public boolean acceptModel() {
        return false;
    }

    public Viewer createViewer(final MainWindow mainWindow, final ActList list) {
        ViewerProxy proxy = new ViewerProxy(NAME, list);
        SortingChooserViewer sortingChooser = new SortingChooserViewer(list, proxy);
        proxy.setViewer(sortingChooser, false);
        return proxy;
    }

    public Viewer createViewer(final MainWindow mainWindow, final ActListFactoryLayout layout) {
        throw new UnsupportedOperationException();
    }
}

