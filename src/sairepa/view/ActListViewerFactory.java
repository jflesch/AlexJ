package sairepa.view;

import sairepa.model.ActList;
import sairepa.model.ActListFactoryLayout;

public class ActListViewerFactory implements ViewerFactory {
    public static final String NAME = "Tableau";

    public ActListViewerFactory() { }

    public String getName() {
        return NAME;
    }

    public boolean acceptActList() {
        return true;
    }

    public boolean acceptModel() {
        return false;
    }

    public Viewer createViewer(final MainWindow mainWindow, final ActList list) {
        assert (list != null);
        return new ActListViewer(list, true /* allow reordering */);
    }

    public Viewer createViewer(final MainWindow mainWindow, final ActListFactoryLayout layout) {
        throw new UnsupportedOperationException();
    }
}
