package sairepa.view;

import java.awt.BorderLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;

import sairepa.model.Act;
import sairepa.model.ActList;

/**
 * @brief Viewer containing another viewer. The contained viewer can be changed
 */
public class ViewerProxy extends Viewer {
    private final static long serialVersionUID = 1;

    private Viewer currentViewer = null;

    public ViewerProxy(String name) {
        this(name, null);
    }

    public ViewerProxy(String name, ActList actList) {
        super(name, actList);
        this.setLayout(new BorderLayout());

        setViewer(null, false);
    }

    public void setViewer(Viewer viewer, boolean maxSize) {
        if (viewer != null) {
            for (ViewerObserver obs : this.getObservers() ) {
                viewer.addObserver(obs);
            }
        }

        this.removeAll();
        if (!maxSize) {
            if (viewer != null) {
                this.add(viewer, BorderLayout.WEST);
            }
            this.add(new JLabel(""), BorderLayout.CENTER);
        } else {
            if (viewer != null) {
                this.add(viewer, BorderLayout.CENTER);
            }
        }

        this.currentViewer = viewer;

        this.validate();
    }

    @Override
    public void init() {
        super.init();
        currentViewer.init();
    }

    @Override
    public void refresh() {
        currentViewer.refresh();
    }

    @Override
    public void refresh(Act a) {
        currentViewer.refresh(a);
    }

    @Override
    public String canClose() {
        return currentViewer.canClose();
    }

    @Override
    public int[] getSelectedActs() {
        return currentViewer.getSelectedActs();
    }

    @Override
    public void setSelectedAct(int act) {
        currentViewer.setSelectedAct(act);
    }

    @Override
    public void addObserver(ViewerObserver obs) {
        super.addObserver(obs);
        currentViewer.addObserver(obs);
    }

    @Override
    public void deleteObserver(ViewerObserver obs) {
        super.deleteObserver(obs);
        currentViewer.deleteObserver(obs);
    }

    @Override
    public void close() {
        super.close();
        currentViewer = null;
    }

    @Override
    public boolean canBePrinted() {
        return currentViewer.canBePrinted();
    }

    @Override
    public JComponent getPrintableComponent() {
        return currentViewer.getPrintableComponent();
    }

    @Override
    public boolean printOnOnePage() {
        return currentViewer.printOnOnePage();
    }

    @Override
    public void printingDone() {
        currentViewer.printingDone();
    }

    @Override
    public boolean canBeSearched() {
        return currentViewer.canBeSearched();
    }

    @Override
    public void displaySearchForm() {
        currentViewer.displaySearchForm();
    }

}
