package sairepa.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import sairepa.gui.IconBox;
import sairepa.model.Act;
import sairepa.model.ActField;
import sairepa.model.ActList;
import sairepa.model.ActSorting;

public class SortingChooserViewer extends Viewer implements ActionListener {
    private static final long serialVersionUID = 1;

    private final ActList actList;
    private final Set<ActField> possibleFields;

    private List<SortingChooser> choosers;
    private JButton addButton;
    private JButton validButton;

    private ViewerProxy proxy;

    public SortingChooserViewer(final ActList al, final ViewerProxy proxy) {
        super(SortedActListViewerFactory.NAME, al);
        this.proxy = proxy;
        this.actList = al;
        choosers = new Vector<SortingChooser>();

        possibleFields = actList.getFactory().getFieldSet();

        SortingChooser chooser = new SortingChooser(this.possibleFields);
        choosers.add(chooser);

        rebuildUI();
    }

    private class SortingChooser {
        private final JComboBox<ActField> fieldChooser;
        private final JCheckBox orderChooser;
        private final JButton removeButton;

        SortingChooser(final Set<ActField> possibleFields) {
            fieldChooser = new JComboBox<ActField>(new Vector<ActField>(possibleFields));
            fieldChooser.setPreferredSize(new java.awt.Dimension(300, 20));
            orderChooser = new JCheckBox("", false);
            removeButton = new JButton(IconBox.remove);
            removeButton.setToolTipText("Retirer ce critère de tri");
        }

        public JComponent getFieldChooser() {
            return fieldChooser;
        }

        public JComponent getOrderChooser() {
            return orderChooser;
        }

        public JButton getRemoveButton() {
            return removeButton;
        }

        public ActSorting getSorting() {
            int index = fieldChooser.getSelectedIndex();
            return new ActSorting(
                    fieldChooser.getItemAt(index),
                    orderChooser.isSelected());
        }
    }

    private void rebuildUI() {
        this.removeAll();
        this.setLayout(new GridLayout(1, 1));

        JPanel omegaPanel = new JPanel();

        omegaPanel.setLayout(new BorderLayout(5, 5));

        JPanel masterPanel = new JPanel(new BorderLayout(5, 5));
        JPanel rightPanel = new JPanel(new BorderLayout(5, 5));

        JPanel orderColumn = new JPanel(new GridLayout(choosers.size() + 3, 1));
        JPanel fieldColumn = new JPanel(new GridLayout(choosers.size() + 3, 1));
        JPanel removeColumn = new JPanel(new GridLayout(choosers.size() + 3, 1));

        /* header */
        fieldColumn.add(new JLabel("Trier par"));
        orderColumn.add(new JLabel("Décroissant"));
        removeColumn.add(new JLabel(""));
        /* choosers */
        int i = 0;
        for (SortingChooser chooser : choosers) {
            orderColumn.add(chooser.getOrderChooser());
            fieldColumn.add(chooser.getFieldChooser());

            if (i == 0) {
                removeColumn.add(new JLabel(""));
            } else {
                removeColumn.add(chooser.getRemoveButton());
                chooser.getRemoveButton().addActionListener(this);
            }
            i++;
        }

        /* validation / adding */
        fieldColumn.add(new JLabel(""));
        orderColumn.add(new JLabel(""));
        this.addButton = new JButton(IconBox.add);
        addButton.setToolTipText("Ajouter un nouveau critère de tri");
        addButton.addActionListener(this);
        removeColumn.add(addButton);

        orderColumn.add(new JLabel(""));
        this.validButton = new JButton("Valider");
        validButton.addActionListener(this);
        fieldColumn.add(validButton);
        removeColumn.add(new JLabel(""));

        masterPanel.add(fieldColumn, BorderLayout.WEST);
        rightPanel.add(orderColumn, BorderLayout.WEST);
        rightPanel.add(removeColumn, BorderLayout.EAST);
        masterPanel.add(rightPanel, BorderLayout.EAST);

        omegaPanel.add(masterPanel, BorderLayout.NORTH);
        omegaPanel.add(new JLabel(""), BorderLayout.CENTER);
        this.add(omegaPanel);

        proxy.validate();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == validButton) {
            Vector<ActSorting> sortingRule = new Vector<ActSorting>();
            for (SortingChooser chooser : choosers) {
                sortingRule.add(chooser.getSorting());
            }
            ActList al = this.actList.getSortedActList(sortingRule);
            proxy.setViewer(new ActListViewer(al, false /* allowReordering */),
                    true /* maxSize */);
        } else if (e.getSource() == addButton) {
            SortingChooser chooser = new SortingChooser(this.possibleFields);
            choosers.add(chooser);
            rebuildUI();
        } else {
            for (SortingChooser chooser : choosers) {
                if (e.getSource() == chooser.getRemoveButton()) {
                    choosers.remove(chooser);
                    rebuildUI();
                    break;
                }
            }
        }
    }

    @Override
    public void refresh() {
        // Nothing to do
    }

    @Override
    public void refresh(Act a) {
        // Nothing to do
    }

    @Override
    public String canClose() {
        return null;
    }

    @Override
    public int[] getSelectedActs() {
        return new int[0];
    }

    @Override
    public void setSelectedAct(int act) {
        throw new UnsupportedOperationException("Can't display act");
    }

    @Override
    public void close() {
        super.close();
        // Nothing to do
    }

    @Override
    public boolean canBePrinted() {
        return false;
    }

    @Override
    public JComponent getPrintableComponent() {
        throw new UnsupportedOperationException("Can't print the sorting selection dialog");
    }

    @Override
    public boolean printOnOnePage() {
        return false;
    }

    @Override
    public void printingDone() {
        super.printingDone();
    }

    @Override
    public boolean canBeSearched() {
        return false;
    }
}
